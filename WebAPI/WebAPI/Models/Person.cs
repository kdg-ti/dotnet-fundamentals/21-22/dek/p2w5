﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Person
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        
        public int Age { get; set; }
    }
}