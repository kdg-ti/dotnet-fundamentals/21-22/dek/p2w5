﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("/[controller]")]
    public class PeopleController : Controller
    {
        private static List<Person> _people;

        static PeopleController()
        {
            _people = new List<Person>()
            {
                new Person() {Id = 1, Name = "Kenneth", Age = 41},
                new Person() {Id = 2, Name = "Agnes", Age = 21}
            };
        }
        /*// GET
        public IActionResult Index()
        {
            return View();
        }*/

        //[HttpGet]
        //[Route("{id}")]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Person p = _people.Find(p => p.Id == id);

            if (p == null)
                return NotFound();
            
            return Ok(p);
        }

        public IActionResult Post([FromBody]Person p)
        {
            if (!ModelState.IsValid)
                return Conflict();

            return CreatedAtAction("Get", new { id= p.Id}, null);
        }
    }
}